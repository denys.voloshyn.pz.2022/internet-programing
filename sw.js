const CACHE_NAME = 'my-pwa-cache';
const urlsToCache = [
    '/',
    '/index.html',
    '/css/style.css',
    '/js/script.js',
];

self.addEventListener('install', function (installEvent) {
    const filesUpdate = cache => {
        const stack = [];
        urlsToCache.forEach(file => stack.push(
            cache.add(file).catch(_ => console.error(`can't load ${file} to cache`))
        ));
        return Promise.all(stack);
    };

    installEvent.waitUntil(caches.open(CACHE_NAME).then(filesUpdate));
});

self.addEventListener('fetch', function (event) {
    event.respondWith(
        caches.match(event.request)
            .then(function (response) {
                if (response) {
                    return response;
                }

                const fetchRequest = event.request.clone();

                return fetch(fetchRequest).then(
                    function (response) {
                        if (!response || response.status !== 200 || response.type !== 'basic') {
                            return response;
                        }

                        const responseToCache = response.clone();

                        caches.open(CACHE_NAME)
                            .then(function (cache) {
                                cache.put(event.request, responseToCache);
                            });

                        return response;
                    }
                );
            })
    );
});
