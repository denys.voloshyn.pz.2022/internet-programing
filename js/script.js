$("#bell").on("mouseover", function () {
  $("#bell-up").css("display", "block");
});

$("#bell-up").on("mouseleave", function () {
  $(this).css("display", "none");
});

$(".account-info img").on("mouseover", function () {
  $("#account-dropdown").css("display", "block");
});

$("#account-dropdown").on("mouseleave", function () {
  $(this).css("display", "none");
});

function clickCheckbox(element) {
  $(element).toggleClass("checked");
  if ($(element).hasClass("checked")) {
    $(element).html(
      `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg>`
    );
  } else {
    $(element).html("");
  }
}

function clickCheckboxAll(element) {
  if ($(element).hasClass("checked")) {
    $(".checkbox").each(function () {
      $(this).removeClass("checked");
      $(this).html("");
    });
  } else {
    $(".checkbox").each(function () {
      $(this).addClass("checked");
      $(this).html(
        `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg>`
      );
    });
  }
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

let selectedRow;
let id_counter = 1;




document.addEventListener('DOMContentLoaded', function() {
  $.ajax({
    url: "./server/get_students.php",
    type: "GET",
    success: function (response) {
      let resp = JSON.parse(response);
      if (resp.status == "success") {
        resp.students.forEach((student) => {
          createStudent(student);
        });

      } else if (resp.status == "error") {
        alert("Error:\n" + resp.errors);
      }
    }
  
});
});




$("#open-add-student-window-button").on("click", function () {
  $("#edit-header").css("display", "none");
  $("#add-header").css("display", "initial");
  $("#student-window").css("display", "initial");
  $("#edit-student-button").css("display", "none");
  $("#create-new-student-button").css("display", "initial");
  $("form").trigger("reset");
});

function closeForm() {
  $("#student-window").css("display", "none");
}

function isFormValid() {
  var group = $("#group").val();
  var firstName = $("#first-name").val();
  var lastName = $("#last-name").val();
  var gender = $("#gender").val();
  var birthday = $("#birthday").val();

  // Перевірка, чи всі поля заповнені
  if (!group || !firstName || !lastName || !gender || !birthday) {
    alert("Please fill in all fields");
    return false;
  }

  // Перевірка, чи ім'я та прізвище складаються тільки з букв
  var letters = /^[A-Z][a-z]+$/;
  if (!firstName.match(letters) || !lastName.match(letters)) {
    alert("Name must start with capital letter and contain only letters");
    return false;
  }

  const date = new Date(birthday);
  const currentDate = new Date();
  const maxDate = new Date();
  const minDate = new Date();

  maxDate.setFullYear(currentDate.getFullYear() - 16);
  minDate.setFullYear(currentDate.getFullYear() - 100);

  if (date > maxDate || date < minDate) {
    alert("Date is not valid");
    return false;
  }
  return true;
}

function createRequest() {
  const mode =
    document.getElementById("add-header").style.display == "initial"
      ? "create"
      : "update";
  const url = "./server/" + mode + "_student.php";
  const data = {
    id: $("#student-id").val(),
    group: $("#group").val(),
    first_name: $("#first-name").val(),
    last_name: $("#last-name").val(),
    gender: $("#gender").val(),
    birthdate: $("#birthday").val(),
  };

  const body = JSON.stringify(data);
  console.log("URL:", url);
  console.log("Body:", body);

  $.ajax({
    url: url,
    type: "POST",
    contentType: "application/json",
    data: body,
    success: function (response) {
      let resp = JSON.parse(response);
      if (resp.status == "success") {
        console.log("SUCCESS!!\n");
        //alert("Data is valid");

        if (document.getElementById("add-header").style.display == "initial") {
          createStudentForm(resp.studentId);
        } else {
          editStudent();
        }
      } else if (resp.status == "error") {
        let errorMessages = "";
        for (let key in resp.errors) {
          if (resp.errors[key]) {
            errorMessages += resp.errors[key] + "\n";
          }
        }
        alert("Error:\n" + errorMessages);
      }
    },
    error: function () {
      console.log("Network error occurred");
    },
  });
}

function editStudent() {
  selectedRow.cells[2].innerText = document.getElementById("group").value;
  selectedRow.cells[3].innerText =
    document.getElementById("first-name").value +
    " " +
    document.getElementById("last-name").value;
  selectedRow.cells[4].innerText = document.getElementById("gender").value;
  selectedRow.cells[5].innerText = convertDateFormat(
    document.getElementById("birthday").value
  );
  closeForm();
}

$("form").on("submit", function (event) {
  event.preventDefault();

  if (isFormValid()) {
    createRequest();
  }
});

function createStudentForm(studentId) {
  // Зберігаємо дані з полів форми
  var group = $("#group").val();
  var firstName = $("#first-name").val();
  var lastName = $("#last-name").val();
  var gender = $("#gender").val();
  var birthday = $("#birthday").val();
  let student = {
    id: studentId,
    group: group,
    first_name: firstName,
    last_name: lastName,
    gender: gender,
    birthdate: birthday,
  }

  createStudent(student);

  closeForm();
}


function createStudent(student) {

  // Створюємо новий рядок та клітинки для таблиці
  var newRow = $("<tr></tr>");
  var newCells = [
    $("<td></td>").addClass("student-id-column").html(student.id),
    $("<td></td>")
      .html('<div class="checkbox" onclick="clickCheckbox(this)"></div>'),
    $("<td></td>").text(student.group),
    $("<td></td>").text(student.first_name + " " + student.last_name),
    $("<td></td>").text(student.gender),
    $("<td></td>").text(convertDateFormat(student.birthdate)),
    $("<td></td>").html(
      '<div class="status-circle status-circle-online"></div>'
    ),
    $("<td></td>").html(
      '<button class="options-button edit-button" onclick="editStudentOnClick(this)"> <img src="images/pencil.svg" alt="Edit" class="options-img" /> </button> <button class="options-button delete-button" onclick="deleteStudentOnClick(this)" > <img src="images/cross.svg" alt="Delete" class="options-img" /> </button>'
    ),
  ];

  // Додаємо клітинки до рядка
  for (var i = 0; i < newCells.length; i++) {
    newRow.append(newCells[i]);
  }

  // Додаємо новий рядок до таблиці
  $(".students-table tbody").append(newRow);

}

function editStudentOnClick(button) {
  document.getElementById("edit-header").style.display = "initial";
  document.getElementById("add-header").style.display = "none";
  document.getElementById("student-window").style.display = "initial";
  document.getElementById("edit-student-button").style.display = "initial";
  document.getElementById("create-new-student-button").style.display = "none";

  selectedRow = button.parentElement.parentElement;

  document.getElementById("student-id").value = selectedRow.cells[0].innerText;
  document.getElementById("group").value = selectedRow.cells[2].innerText;
  document.getElementById("first-name").value =
    selectedRow.cells[3].innerText.split(" ")[0];
  document.getElementById("last-name").value =
    selectedRow.cells[3].innerText.split(" ")[1];
  document.getElementById("gender").value = selectedRow.cells[4].innerText;
  document.getElementById("birthday").value = reverseConvertDateFormat(
    selectedRow.cells[5].innerText
  );
}

function deleteStudent(studentId) {
  body = JSON.stringify({ id: studentId });
  $.ajax({
    url: "./server/delete_student.php",
    type: "POST",
    contentType: "application/json",
    data: body,
    success: function (response) {
      let resp = JSON.parse(response);
      if (resp.status == "success") {
        console.log("SUCCESS!!\n");
      } else if (resp.status == "error") {
        alert("Error:\n" + resp.errors);
      }
    },
    error: function () {
      console.log("Network error occurred");
    },
  });
}

function deleteStudentOnClick(button) {
  // Show the confirmation window
  $("#confirmation-window").css("display", "flex");

  // Add event listeners to the confirmation and cancellation buttons
  $("#confirm-delete-button").on("click", function () {
    let rowToDelete = $(button).parent().parent();
    let studentId = rowToDelete.find(".student-id-column").text();
    deleteStudent(studentId);
    rowToDelete.remove();

    // Hide the confirmation window
    $("#confirmation-window").css("display", "none");
  });

  $("#cancel-delete-button").on("click", function () {
    // Hide the confirmation window
    $("#confirmation-window").css("display", "none");
  });
}

function convertDateFormat(inputDate) {
  let datePart = inputDate.split("-");
  let year = datePart[0];
  let month = datePart[1];
  let day = datePart[2];

  return day + "." + month + "." + year;
}

function reverseConvertDateFormat(inputDate) {
  let datePart = inputDate.split(".");
  let day = datePart[0];
  let month = datePart[1];
  let year = datePart[2];

  return year + "-" + month + "-" + day;
}
