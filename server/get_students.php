<?php

include 'db.php';

$db_connection = get_db_conection();
if(!$db_connection){
    echo json_encode(array('status' => 'error', 'errors' => 'Database connection error.' + mysqli_connect_error()));
    exit(0);
}

$sql_select_query = "SELECT * FROM student";
$result = $db_connection->query($sql_select_query);

if ($result->num_rows > 0) {
    $data = array();
    
    while($row = $result->fetch_row()) {
        $row_to_data = array(
            'id' => $row[0],
            'first_name' => $row[1],
            'last_name' => $row[2],
            'group' => $row[3],
            'gender' => $row[4],
            'birthdate' => $row[5]);
        array_push($data, $row_to_data);
    }
    
    echo json_encode(array('status' => 'success', 'students' => $data));
} else {
    echo json_encode(array('status' => 'empty', 'errors' => 'No students found'));
}

$db_connection->close();
