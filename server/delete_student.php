<?php

include 'db.php';

$data = json_decode(file_get_contents('php://input'), true);

$db_connection = get_db_conection();

if(!$db_connection){
    echo json_encode(array('status' => 'error', 'errors' => 'Database connection error.' + mysqli_connect_error()));
    exit(0);
}

$sql_create_query = $db_connection->prepare("DELETE FROM student WHERE student.id = ?");
$sql_create_query->bind_param("i" ,$data['id']);

if($sql_create_query->execute()){
    echo json_encode(array('status' => 'success'));
} else {
    echo json_encode(array('status' => 'error', 'errors' => 'Database error.' + $db_connection->error));
}

$db_connection->close();
exit(0);