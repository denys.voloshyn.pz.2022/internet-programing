<?php

include 'form_validation.php';
include 'db.php';

$data = json_decode(file_get_contents('php://input'), true);


$errors =  validate_student($data);

if (is_erorrs_present($errors)){
    echo json_encode(array('status' => 'error', 'errors' => $errors));
    exit(0);
}

$db_connection = get_db_conection();

if(!$db_connection){
    echo json_encode(array('status' => 'error', 'errors' => 'Database connection error.' + mysqli_connect_error()));
    exit(0);
}

$sql_create_query = $db_connection->prepare("INSERT INTO student (first_name, last_name, class, gender, birthdate) VALUES (?, ?, ?, ?, ?)");
$sql_create_query->bind_param("sssss" ,$data['first_name'], $data['last_name'], $data['group'], $data['gender'], $data['birthdate']);

if($sql_create_query->execute()){
    echo json_encode(array('status' => 'success' , 'studentId' => $db_connection->insert_id));
} else {
    echo json_encode(array('status' => 'error', 'errors' => 'Database error.' + $db_connection->error));
}
$db_connection->close();

exit(0);
