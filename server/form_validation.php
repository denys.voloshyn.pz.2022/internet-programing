<?php


function validate_student($data)
{
    $errors = array(
        'group' => validateGroup($data['group'] ),
        'first_name' => validateFirstName($data['first_name']),
        'last_name' => validateLastName($data['last_name']),
        'birthdate' => validateBirthdate($data['birthdate']),
        'gender' => validateGender($data['gender'] )
    );

    return $errors;
}

function validateGroup($group)
{
    $errorMessage = '';
    if (empty($group)) {
        $errorMessage = 'Group is required';
    } else if (!preg_match('/^PZ-2[1-7]$/', $group)) {
        $errorMessage = 'Group format is invalid. ';
    }
    return $errorMessage;
}

function validateFirstName($first_name)
{
    $errorMessage = '';
    if (empty($first_name)) {
        $errorMessage = 'Name is required';
    } else if(strlen($first_name) < 2) {
        $errorMessage = 'Name must contains atleast 2 characters';
    } else if (!preg_match('/^[A-Z][a-z]+$/', $first_name)) {
        $errorMessage = 'Name must start with a capital letter and contain only letters';
    }
    return $errorMessage;
}

function validateLastName($last_name)
{
    $errorMessage = '';
    if (empty($last_name)) {
        $errorMessage = 'Last name is required';
    } else if(strlen($last_name) < 2) {
        $errorMessage = 'Last name must contains atleast 2 characters';
    } else if (!preg_match('/^[A-Z][a-z]+$/', $last_name)) {
        $errorMessage = 'Last name must start with a capital letter and contain only letters';
    } 
    return $errorMessage;
}


function validateGender($gender)
{
    $errorMessage = '';
    if (empty($gender)) {
        $errorMessage = 'Gender is required';
    } else if($gender !='M' && $gender != 'F') {
        $errorMessage = 'Gender format is invalid.';
    }
    return $errorMessage;
}

function validateBirthdate($birthdate)
{
    $errorMessage = '';
    if (empty($birthdate)) {
        $errorMessage = 'Birthday is required';
    } else if (!preg_match('/^\d{4}-\d{2}-\d{2}$/', $birthdate)) {
        $errorMessage = 'Birthday must be in format YYYY-MM-DD';
    } else if(strtotime($birthdate) > time()) {
        $errorMessage = 'Birthday cannot be in the future';
    } else if(strtotime($birthdate) > strtotime('-16 years')) {
        $errorMessage = 'You must be at least 16 years old';
    } else if(strtotime($birthdate) < strtotime('-100 years')) {
        $errorMessage = 'You must be no more than 100 years old';
    }
    return $errorMessage;
}


function is_erorrs_present($errors)
{
    return !empty($errors['group']) || !empty($errors['first_name']) || !empty($errors['last_name']) || !empty($errors['birthdate']) || !empty($errors['gender']);
}
